package tk.facturadmin.safe.adapters;

import java.util.List;

import tk.facturadmin.safe.models.Factura;

/**
 * Created by joeldmtz on 7/23/16.
 */
public interface FacturaListener {
    void onItemUnSelected(int position);
    void onDataserUpdated(List<Factura> facturas_set);
    void onFetch();
}
