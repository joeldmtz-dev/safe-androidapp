package tk.facturadmin.safe.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.activities.FacturaActivity;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.Factura;
import tk.facturadmin.safe.models.FacturaCompleta;
import tk.facturadmin.safe.models.requests.FacturaSeleccionada;
import tk.facturadmin.safe.models.requests.Ok;

/**
 * Created by joeldmtz on 7/20/16.
 */
public class FacturaCardAdapter extends RecyclerView.Adapter<FacturaCardAdapter.ViewHolder> {
    private List<Factura> mDataset;
    private Context context;
    private FacturaListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView folio, fecha, emisor, receptor, importe, traslados;
        private ImageButton selected;
        private int id;

        public ViewHolder(View itemView) {
            super(itemView);
            folio = (TextView) itemView.findViewById(R.id.folio_val);
            fecha = (TextView) itemView.findViewById(R.id.fecha_val);
            emisor = (TextView) itemView.findViewById(R.id.emisor_val);
            receptor = (TextView) itemView.findViewById(R.id.receptor_val);
            importe = (TextView) itemView.findViewById(R.id.importe_val);
            traslados = (TextView) itemView.findViewById(R.id.impuestos_val);
            selected = (ImageButton) itemView.findViewById(R.id.btn_selected);
        }
    }

    public FacturaCardAdapter(Context context, List<Factura> mDataset, FacturaListener listener) {
        this.mDataset = mDataset;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_factura, parent, false);

        final ViewHolder vh = new ViewHolder(v);

        final ImageButton selected = (ImageButton) v.findViewById(R.id.btn_selected);
        selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = vh.getAdapterPosition();
                int seleccionado = mDataset.get(position).getSeleccionado();
                final int seleccionar = (seleccionado == 1)?0:1;

                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, context);
                Call<Ok> call = service.seleccionarFactura(
                        new FacturaSeleccionada(
                                mDataset.get(position).getId(),
                                seleccionar
                        )
                );

                call.enqueue(new Callback<Ok>() {
                    @Override
                    public void onResponse(Call<Ok> call, Response<Ok> response) {
                        if(response.code() == 200){
                            mDataset.get(position).setSeleccionado(seleccionar);
                            if(seleccionar == 1){
                                selected.setColorFilter(context.getResources().getColor(R.color.green_checked));
                            } else {
                                selected.setColorFilter(context.getResources().getColor(R.color.gray_unchecked));
                                listener.onItemUnSelected(position);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Ok> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        });

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, context);
                final int position = vh.getAdapterPosition();
                Call<FacturaCompleta> call = service.getFactura(mDataset.get(position).getId());
                call.enqueue(new Callback<FacturaCompleta>() {
                    @Override
                    public void onResponse(Call<FacturaCompleta> call, Response<FacturaCompleta> response) {
                        if(response.code() == 200){
                            //((Activity) context).finish();
                            Intent intent = new Intent();
                            intent.setClass(context, FacturaActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("factura", response.body());
                            mBundle.putSerializable("factura_simple", mDataset.get(position));
                            mBundle.putInt("seleccionado", mDataset.get(position).getSeleccionado());
                            mBundle.putInt("position", position);
                            intent.putExtras(mBundle);
                            context.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<FacturaCompleta> call, Throwable t) {

                    }
                });
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.id = mDataset.get(position).getId();
        holder.folio.setText(String.valueOf(mDataset.get(position).getFolio()));
        holder.fecha.setText(mDataset.get(position).getFecha());
        holder.emisor.setText(mDataset.get(position).getEmisor());
        holder.receptor.setText(mDataset.get(position).getReceptor());
        holder.importe.setText(String.valueOf(mDataset.get(position).getTotal()));
        holder.traslados.setText(String.valueOf(mDataset.get(position).getTotalImpuestosTrasladados()));

        if(mDataset.get(position).getSeleccionado() == 1){
            holder.selected.setColorFilter(context.getResources().getColor(R.color.green_checked));
        } else {
            holder.selected.setColorFilter(context.getResources().getColor(R.color.gray_unchecked));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
