package tk.facturadmin.safe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tk.facturadmin.safe.R;
import tk.facturadmin.safe.models.Empresa;

/**
 * Created by joeldmtz on 7/23/16.
 */
public class EmpresaAdapter extends ArrayAdapter{
    List<Empresa> mDataset;
    int layout;
    Context context;

    public EmpresaAdapter(Context context, int resource, List<Empresa> mDataset) {
        super(context, resource);
        this.context = context;
        this.layout = resource;
        this.mDataset = mDataset;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item = inflater.inflate(this.layout, parent, false);
        TextView label = (TextView) item.findViewById(android.R.id.text1);
        label.setText(mDataset.get(position).getNombre());
        return item;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }
}
