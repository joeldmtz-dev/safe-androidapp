package tk.facturadmin.safe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tk.facturadmin.safe.R;
import tk.facturadmin.safe.models.Reporte;

/**
 * Created by joeldmtz on 7/24/16.
 */
public class EmpresaReporteAdapter extends ArrayAdapter {
    private List<Reporte.ReporteEmpresa> mDataset;
    private int layout;
    private Context context;


    public EmpresaReporteAdapter(Context context, int resource, List<Reporte.ReporteEmpresa> mDataset) {
        super(context, resource);
        this.mDataset = mDataset;
        this.context = context;
        this.layout = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item = inflater.inflate(this.layout, parent, false);

        TextView empresa_nombre = (TextView) item.findViewById(R.id.empresa_nombre_val);
        TextView empresa_facturas = (TextView) item.findViewById(R.id.empresa_facturas_val);
        TextView empresa_importe = (TextView) item.findViewById(R.id.empresa_importe_val);
        TextView empresa_impuestos = (TextView) item.findViewById(R.id.empresa_traslados_val);

        empresa_nombre.setText(mDataset.get(position).getNombre());
        empresa_facturas.setText(String.valueOf(mDataset.get(position).getTotalFacturas()));
        empresa_importe.setText(String.valueOf(mDataset.get(position).getImporte()));
        empresa_impuestos.setText(String.valueOf(mDataset.get(position).getImpuestos()));

        return item;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }
}
