package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class Concepto implements Serializable{
    private int id;
    private double cantidad;
    private String unidad;
    private String descripcion;
    private double valorUnitario;
    private double importe;

    public Concepto() {
    }

    public Concepto(int id, double cantidad, String unidad, String descripcion, double valorUnitario, double importe) {
        this.id = id;
        this.cantidad = cantidad;
        this.unidad = unidad;
        this.descripcion = descripcion;
        this.valorUnitario = valorUnitario;
        this.importe = importe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
}
