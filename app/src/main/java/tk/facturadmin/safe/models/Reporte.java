package tk.facturadmin.safe.models;

import java.util.List;

/**
 * Created by joeldmtz on 7/24/16.
 */
public class Reporte {

    public static class ReporteEmpresa{
        private String nombre;
        private int totalFacturas;
        private double importe;
        private double impuestos;

        public ReporteEmpresa() {
        }

        public ReporteEmpresa(String nombre, int totalFacturas, double importe, double impuestos) {
            this.nombre = nombre;
            this.totalFacturas = totalFacturas;
            this.importe = importe;
            this.impuestos = impuestos;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public int getTotalFacturas() {
            return totalFacturas;
        }

        public void setTotalFacturas(int totalFacturas) {
            this.totalFacturas = totalFacturas;
        }

        public double getImporte() {
            return importe;
        }

        public void setImporte(double importe) {
            this.importe = importe;
        }

        public double getImpuestos() {
            return impuestos;
        }

        public void setImpuestos(double impuestos) {
            this.impuestos = impuestos;
        }
    }

    private int month;
    private int year;
    private int totalFacturas;
    private double importe;
    private double impuestos;
    private List<ReporteEmpresa> empresas;

    public Reporte() {
    }

    public Reporte(int month, int year, int totalFacturas, double importe, double impuestos) {
        this.month = month;
        this.year = year;
        this.totalFacturas = totalFacturas;
        this.importe = importe;
        this.impuestos = impuestos;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getTotalFacturas() {
        return totalFacturas;
    }

    public void setTotalFacturas(int totalFacturas) {
        this.totalFacturas = totalFacturas;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public double getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(double impuestos) {
        this.impuestos = impuestos;
    }

    public List<ReporteEmpresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<ReporteEmpresa> empresas) {
        this.empresas = empresas;
    }
}

