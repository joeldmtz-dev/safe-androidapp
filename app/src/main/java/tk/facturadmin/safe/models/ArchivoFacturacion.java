package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class ArchivoFacturacion implements Serializable{
    private  int id;
    private String xml;
    private String pdf;

    public ArchivoFacturacion() {
    }

    public ArchivoFacturacion(int id, String xml, String pdf) {
        this.id = id;
        this.xml = xml;
        this.pdf = pdf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
