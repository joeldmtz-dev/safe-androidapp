package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/20/16.
 */
public class Factura implements Serializable{
    private int id;
    private int folio;
    private String fecha;
    private String emisor;
    private String receptor;
    private double total;
    private double totalImpuestosTrasladados;
    private int seleccionado;
    private String pdf;

    public Factura() {
    }

    public Factura(int id, int folio, String fecha, String emisor, String receptor, double total, double totalImpuestosTrasladados, int seleccionado, String pdf) {
        this.id = id;
        this.folio = folio;
        this.fecha = fecha;
        this.emisor = emisor;
        this.receptor = receptor;
        this.total = total;
        this.totalImpuestosTrasladados = totalImpuestosTrasladados;
        this.seleccionado = seleccionado;
        this.pdf = pdf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getReceptor() {
        return receptor;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotalImpuestosTrasladados() {
        return totalImpuestosTrasladados;
    }

    public void setTotalImpuestosTrasladados(double totalImpuestosTrasladados) {
        this.totalImpuestosTrasladados = totalImpuestosTrasladados;
    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(int seleccionado) {
        this.seleccionado = seleccionado;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
