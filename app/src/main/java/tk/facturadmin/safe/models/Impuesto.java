package tk.facturadmin.safe.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class Impuesto implements Serializable{
    private int id;
    private double totalImpuestosTrasladados;
    private List<Traslado> traslados = new ArrayList<>();

    public Impuesto() {
    }

    public Impuesto(int id, double totalImpuestosTrasladados, List<Traslado> traslados) {
        this.id = id;
        this.totalImpuestosTrasladados = totalImpuestosTrasladados;
        this.traslados = traslados;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalImpuestosTrasladados() {
        return totalImpuestosTrasladados;
    }

    public void setTotalImpuestosTrasladados(double totalImpuestosTrasladados) {
        this.totalImpuestosTrasladados = totalImpuestosTrasladados;
    }

    public List<Traslado> getTraslados() {
        return traslados;
    }

    public void setTraslados(List<Traslado> traslados) {
        this.traslados = traslados;
    }
}
