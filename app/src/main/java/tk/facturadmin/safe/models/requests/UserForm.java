package tk.facturadmin.safe.models.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joeldmtz on 7/19/16.
 */
public class UserForm {

    @SerializedName("username")
    String username;

    @SerializedName("password")
    String password;

    @SerializedName("newPassword")
    String newPassword;

    public UserForm(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserForm(String username, String password, String newPassword) {
        this.password = password;
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return username;
    }
}
