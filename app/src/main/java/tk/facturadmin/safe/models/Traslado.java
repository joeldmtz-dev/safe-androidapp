package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class Traslado implements Serializable{
    private int id;
    private String impuesto;
    private double tasa;
    private double importe;

    public Traslado() {
    }

    public Traslado(int id, String impuesto, double tasa, double importe) {
        this.id = id;
        this.impuesto = impuesto;
        this.tasa = tasa;
        this.importe = importe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
}
