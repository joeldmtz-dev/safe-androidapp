package tk.facturadmin.safe.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class FacturaCompleta implements Serializable{

    private int id;
    private int folio;
    private Date fecha;
    private String formaDePago;
    private String noCertificado;
    private String certificado;
    private String sello;
    private String condicionesDePago;
    private double tipoCambio;
    private String moneda;
    private double subTotal;
    private double descuento;
    private double total;
    private String tipoDeComprobante;
    private String metodoDePago;
    private String lugarExpedicion;
    private Empresa emisor = new Empresa();
    private Empresa cliente = new Empresa();
    private List<Concepto> conceptos = new ArrayList<>();
    private Impuesto impuestos = new Impuesto();
    private ArchivoFacturacion file = new ArchivoFacturacion();

    public FacturaCompleta() {
    }

    public FacturaCompleta(int id, int folio, Date fecha, String formaDePago, String noCertificado, String certificado, String sello, String condicionesDePago, double tipoCambio, String moneda, double subTotal, double descuento, double total, String tipoDeComprobante, String metodoDePago, String lugarExpedicion, Empresa emisor, Empresa cliente, List<Concepto> conceptos, Impuesto impuestos, ArchivoFacturacion file) {
        this.id = id;
        this.folio = folio;
        this.fecha = fecha;
        this.formaDePago = formaDePago;
        this.noCertificado = noCertificado;
        this.certificado = certificado;
        this.sello = sello;
        this.condicionesDePago = condicionesDePago;
        this.tipoCambio = tipoCambio;
        this.moneda = moneda;
        this.subTotal = subTotal;
        this.descuento = descuento;
        this.total = total;
        this.tipoDeComprobante = tipoDeComprobante;
        this.metodoDePago = metodoDePago;
        this.lugarExpedicion = lugarExpedicion;
        this.emisor = emisor;
        this.cliente = cliente;
        this.conceptos = conceptos;
        this.impuestos = impuestos;
        this.file = file;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFormaDePago() {
        return formaDePago;
    }

    public void setFormaDePago(String formaDePago) {
        this.formaDePago = formaDePago;
    }

    public String getNoCertificado() {
        return noCertificado;
    }

    public void setNoCertificado(String noCertificado) {
        this.noCertificado = noCertificado;
    }

    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public String getCondicionesDePago() {
        return condicionesDePago;
    }

    public void setCondicionesDePago(String condicionesDePago) {
        this.condicionesDePago = condicionesDePago;
    }

    public double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getTipoDeComprobante() {
        return tipoDeComprobante;
    }

    public void setTipoDeComprobante(String tipoDeComprobante) {
        this.tipoDeComprobante = tipoDeComprobante;
    }

    public String getMetodoDePago() {
        return metodoDePago;
    }

    public void setMetodoDePago(String metodoDePago) {
        this.metodoDePago = metodoDePago;
    }

    public String getLugarExpedicion() {
        return lugarExpedicion;
    }

    public void setLugarExpedicion(String lugarExpedicion) {
        this.lugarExpedicion = lugarExpedicion;
    }

    public Empresa getEmisor() {
        return emisor;
    }

    public void setEmisor(Empresa emisor) {
        this.emisor = emisor;
    }

    public Empresa getCliente() {
        return cliente;
    }

    public void setCliente(Empresa cliente) {
        this.cliente = cliente;
    }

    public List<Concepto> getConceptos() {
        return conceptos;
    }

    public void setConceptos(List<Concepto> conceptos) {
        this.conceptos = conceptos;
    }

    public Impuesto getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(Impuesto impuestos) {
        this.impuestos = impuestos;
    }

    public ArchivoFacturacion getFile() {
        return file;
    }

    public void setFile(ArchivoFacturacion file) {
        this.file = file;
    }
}
