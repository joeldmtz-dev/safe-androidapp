package tk.facturadmin.safe.models.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joeldmtz on 7/22/16.
 */
public class FacturaSeleccionada {

    @SerializedName("id")
    private int id;
    @SerializedName("seleccionado")
    private int seleccionado;

    public FacturaSeleccionada(int id, int seleccionado) {
        this.id = id;
        this.seleccionado = seleccionado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(int seleccionado) {
        this.seleccionado = seleccionado;
    }
}
