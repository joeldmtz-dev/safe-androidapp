package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class Empresa implements Serializable{
    private int id;
    private String rfc;
    private String nombre;
    private Domicilio domicilio;
    private String regimenFiscal;

    public Empresa() {
    }

    public Empresa(int id, String rfc, String nombre, Domicilio domicilio, String regimenFiscal) {
        this.id = id;
        this.rfc = rfc;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.regimenFiscal = regimenFiscal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public String getRegimenFiscal() {
        return regimenFiscal;
    }

    public void setRegimenFiscal(String regimenFiscal) {
        this.regimenFiscal = regimenFiscal;
    }
}
