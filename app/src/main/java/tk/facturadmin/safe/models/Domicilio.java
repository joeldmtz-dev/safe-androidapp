package tk.facturadmin.safe.models;

import java.io.Serializable;

/**
 * Created by joeldmtz on 7/21/16.
 */
public class Domicilio implements Serializable{
    private int id;
    private String calle;
    private String noExterior;
    private String colonia;
    private String localidad;
    private String municipio;
    private String estado;
    private String pais;
    private int codigoPostal;

    public Domicilio() {
    }

    public Domicilio(int id, String calle, String noExterior, String colonia, String localidad, String municipio, String estado, String pais, int codigoPostal) {
        this.id = id;
        this.calle = calle;
        this.noExterior = noExterior;
        this.colonia = colonia;
        this.localidad = localidad;
        this.municipio = municipio;
        this.estado = estado;
        this.pais = pais;
        this.codigoPostal = codigoPostal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNoExterior() {
        return noExterior;
    }

    public void setNoExterior(String noExterior) {
        this.noExterior = noExterior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getDomicilio(){
        return calle + " " + noExterior + " " + colonia + ", " +
                municipio + ", " + estado + ", " + pais + ", " + codigoPostal;
    }
}
