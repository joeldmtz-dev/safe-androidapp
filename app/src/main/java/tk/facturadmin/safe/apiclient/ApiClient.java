package tk.facturadmin.safe.apiclient;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by joeldmtz on 7/19/16.
 */
public class ApiClient{
    private Gson gson;
    OkHttpClient.Builder httpclient;
    private Retrofit.Builder builder;

    public ApiClient(String endpoint) {
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

        httpclient = new OkHttpClient.Builder();

        builder = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    public <S> S getService(Class<S> serviceClass, Context context){
        httpclient.addInterceptor(new RequestInterceptor(context));
        Retrofit retrofit = builder.client(httpclient.build()).build();
        return retrofit.create(serviceClass);
    }
}
