package tk.facturadmin.safe.apiclient;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.Call;
import retrofit2.http.Path;
import tk.facturadmin.safe.models.Empresa;
import tk.facturadmin.safe.models.Factura;
import tk.facturadmin.safe.models.FacturaCompleta;
import tk.facturadmin.safe.models.Reporte;
import tk.facturadmin.safe.models.requests.FacturaSeleccionada;
import tk.facturadmin.safe.models.requests.Ok;
import tk.facturadmin.safe.models.requests.UserForm;

/**
 * Created by joeldmtz on 7/19/16.
 */
public interface SafeAPI {
    String ENDPOINT = "https://facturadmin.tk";

    @POST("/user/login")
    Call<Ok> login(@Body UserForm form);

    @GET("/user/logout/logout")
    Call<Ok> logout();

    @GET("/user/login/check")
    Call<Ok> checkLogin();

    @GET("/facturas/empresas")
    Call<List<Empresa>> getEmpresas();

    @GET("/facturas/data")
    Call<List<Factura>> getFacturas();

    @GET("/facturas/factura/{id}")
    Call<FacturaCompleta> getFactura(@Path("id") int id);

    @POST("/facturas/seleccionar")
    Call<Ok> seleccionarFactura(@Body FacturaSeleccionada factura);

    @GET("/facturas/seleccionados")
    Call<List<Factura>> getFacturasSeleccionadas();

    @GET("/facturas/empresa/{id}")
    Call<List<Factura>> getFacturasByEmpresa(@Path("id") int id);

    @GET("/facturas/fecha/{fecha}")
    Call<List<Factura>> getFacturasByDate(@Path("fecha") String fecha);

    @GET("/facturas/dowload_selected_path")
    Call<Ok> getSelectedPdfPath();

    @GET("/facturas/years")
    Call<List<String>> getYears();

    @GET("/facturas/reportes/{year}/{month}")
    Call<Reporte> getReporte(@Path("year") String year, @Path("month") int month);

    @POST("/user/change")
    Call<Ok> changePassword(@Body UserForm form);
}
