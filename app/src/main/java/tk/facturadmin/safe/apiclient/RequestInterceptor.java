package tk.facturadmin.safe.apiclient;

import android.content.Context;

import com.loopj.android.http.PersistentCookieStore;

import java.io.IOException;
import java.util.List;

import cz.msebera.android.httpclient.client.methods.RequestBuilder;
import cz.msebera.android.httpclient.cookie.Cookie;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by joeldmtz on 7/19/16.
 */
public class RequestInterceptor implements Interceptor {

    private Context context;

    public RequestInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        PersistentCookieStore store = new PersistentCookieStore(this.context);
        List<Cookie> cookies = store.getCookies();

        if(cookies.size() == 0){
            return chain.proceed(request);
        }
        Request.Builder builder = request.newBuilder();

        for (Cookie cookie : cookies) {
            if(cookie != null){
                builder.addHeader(cookie.getName(), cookie.getValue());
            }
        }

        return chain.proceed(builder.build());
    }
}
