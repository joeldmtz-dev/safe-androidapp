package tk.facturadmin.safe.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.activities.MainActivity;
import tk.facturadmin.safe.adapters.FacturaListener;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.Factura;

/**
 * Created by joeldmtz on 7/23/16.
 */
public class FechaFormFragment extends DialogFragment {
    private FacturaListener listener;

    public void setListener(FacturaListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.fragment_fecha_form, null);
        final DatePicker date_pick = (DatePicker) view.findViewById(R.id.fecha_pick);

        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(date_pick.getYear(), date_pick.getMonth(), date_pick.getDayOfMonth());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        MainActivity.date_filter = formatter.format(calendar.getTime());
                        listener.onFetch();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FechaFormFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
}
