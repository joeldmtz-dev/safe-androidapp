package tk.facturadmin.safe.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.PersistentCookieStore;

import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.activities.MainActivity;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.models.requests.Ok;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.requests.UserForm;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment{


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button btnLogin = (Button) view.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button login = (Button) getActivity().findViewById(R.id.btn_login);
                UserForm form = validate();
                if(form != null){
                    login.setEnabled(false);
                    final ProgressDialog progressDialog = new ProgressDialog(
                            getActivity(),
                            R.style.AppTheme_Dark_Dialog
                    );
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Authenticating...");
                    progressDialog.show();

                    ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                    final SafeAPI service = client.getService(SafeAPI.class, getActivity());
                    Call<Ok> call = service.login(form);
                    call.enqueue(new Callback<Ok>() {
                        @Override
                        public void onResponse(Call<Ok> call, Response<Ok> response) {
                            if(response.code() == 200){
                                PersistentCookieStore store = new PersistentCookieStore(getActivity());
                                store.clear();
                                BasicClientCookie cookie = new BasicClientCookie("Cookie", response.headers().get("Set-Cookie"));
                                cookie.setVersion(1);
                                cookie.setDomain(response.headers().get("Host"));
                                cookie.setPath("/");
                                store.addCookie(cookie);

                                progressDialog.dismiss();
                                getActivity().finish();
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), MainActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<Ok> call, Throwable t) {
                            t.printStackTrace();
                            System.out.println("Fallo >>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    });
                }
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        return view;
    }

    public UserForm validate(){
        boolean valid = true;

        TextInputLayout lay_username = (TextInputLayout) getActivity().findViewById(R.id.input_lay_username);
        TextInputLayout lay_pass = (TextInputLayout) getActivity().findViewById(R.id.input_lay_pass);
        EditText username = (EditText) getActivity().findViewById(R.id.edt_username);
        EditText password = (EditText) getActivity().findViewById(R.id.edt_password);

        if(username.getText().toString().isEmpty()){
            valid = false;
            lay_username.setError("Campo requerido");
        }

        if(password.getText().toString().isEmpty()){
            valid = false;
            lay_pass.setError("Campo requerido");
        }

        if(valid){
            return new UserForm(username.getText().toString(), password.getText().toString());
        }

        return null;
    }

    public void clearValidation(){
        TextInputLayout lay_username = (TextInputLayout) getActivity().findViewById(R.id.input_lay_username);
        TextInputLayout lay_pass = (TextInputLayout) getActivity().findViewById(R.id.input_lay_pass);
        lay_username.setError(null);
        lay_pass.setError(null);
    }
}
