package tk.facturadmin.safe.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.requests.Ok;

public class SplashActivity extends AppCompatActivity {
    private static final long SPLASH_SCREEN_DELAY = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, SplashActivity.this);
                Call<Ok> call_check = service.checkLogin();
                call_check.enqueue(new Callback<Ok>() {
                    @Override
                    public void onResponse(Call<Ok> call, Response<Ok> response) {
                        finish();
                        Intent intent = new Intent();
                        if(response.code() == 200){
                            intent.setClass(SplashActivity.this, MainActivity.class);
                        } else {
                            intent.setClass(SplashActivity.this, LoginActivity.class);
                        }
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<Ok> call, Throwable t) {

                    }
                });
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);

    }
}
