package tk.facturadmin.safe.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.requests.Ok;
import tk.facturadmin.safe.models.requests.UserForm;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        final Button btn_cambiar = (Button) findViewById(R.id.btn_cambiar);
        btn_cambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserForm form = validate();

                if(form != null){
                    btn_cambiar.setEnabled(false);
                    final ProgressDialog progressDialog = new ProgressDialog(
                            SettingsActivity.this
                    );
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Procesando...");
                    progressDialog.show();



                    ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                    final SafeAPI service = client.getService(SafeAPI.class, SettingsActivity.this);
                    Call<Ok> call = service.changePassword(form);
                    call.enqueue(new Callback<Ok>() {
                        @Override
                        public void onResponse(Call<Ok> call, Response<Ok> response) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                            if(response.code() == 200){
                                builder.setTitle("Operación exitosa")
                                        .setMessage("Contraseña actualizada correctamente");
                            } else {
                                builder.setTitle("Operación fallida")
                                        .setMessage("La contraseña actual proporcionada no es correcta")
                                        .setIcon(android.R.drawable.ic_dialog_alert);
                            }

                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                            btn_cambiar.setEnabled(true);
                            clear();
                        }

                        @Override
                        public void onFailure(Call<Ok> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    public UserForm validate(){
        clearValidation();
        boolean valid = true;

        TextInputLayout lay_current_pass = (TextInputLayout) findViewById(R.id.input_lay_current_pass);
        TextInputLayout lay_new_pass = (TextInputLayout) findViewById(R.id.input_lay_new_pass);
        TextInputLayout lay_re_new_pass = (TextInputLayout) findViewById(R.id.input_lay_re_new_pass);
        EditText current_password = (EditText) findViewById(R.id.edt_current_pass);
        EditText new_password = (EditText) findViewById(R.id.edt_new_password);
        EditText re_new_password = (EditText) findViewById(R.id.edt_re_new_password);

        if(current_password.getText().toString().isEmpty()){
            valid = false;
            lay_current_pass.setError("Campo requerido");
        }

        if(new_password.getText().toString().isEmpty()){
            valid = false;
            lay_new_pass.setError("Campo requerido");
        }
        if(re_new_password.getText().toString().isEmpty()){
            valid = false;
            lay_re_new_pass.setError("Campo requerido");
        }

        if(!new_password.getText().toString().equals(re_new_password.getText().toString())){
            valid = false;
            lay_re_new_pass.setError("La contraseña debe coincidir");
        }

        if(valid){
            return new UserForm(
                    null,
                    current_password.getText().toString(),
                    new_password.getText().toString()
            );

        }

        return null;
    }

    public void clearValidation(){
        TextInputLayout lay_current_pass = (TextInputLayout) findViewById(R.id.input_lay_current_pass);
        TextInputLayout lay_new_pass = (TextInputLayout) findViewById(R.id.input_lay_new_pass);
        TextInputLayout lay_re_new_pass = (TextInputLayout) findViewById(R.id.input_lay_re_new_pass);
        lay_current_pass.setError(null);
        lay_new_pass.setError(null);
        lay_re_new_pass.setError(null);
    }

    public void clear(){
        EditText current_password = (EditText) findViewById(R.id.edt_current_pass);
        EditText new_password = (EditText) findViewById(R.id.edt_new_password);
        EditText re_new_password = (EditText) findViewById(R.id.edt_re_new_password);
        current_password.setText("");
        new_password.setText("");
        re_new_password.setText("");
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
