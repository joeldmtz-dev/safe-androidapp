package tk.facturadmin.safe.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.adapters.EmpresaReporteAdapter;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.Reporte;

public class ReportActivity extends AppCompatActivity {
    public static Reporte report = null;
    public static List<String> years = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Button btn_mostrar = (Button) findViewById(R.id.btn_mostrar);
        final Spinner spinner_mes = (Spinner) findViewById(R.id.spinner_mes);
        final Spinner spinner_anio = (Spinner) findViewById(R.id.spinner_anio);

        final TextView total_facturas = (TextView) findViewById(R.id.total_facturas_val);
        final TextView total_importe = (TextView) findViewById(R.id.total_importe_val);
        final TextView total_traslados = (TextView) findViewById(R.id.total_traslados_val);
        final ListView empresas_reporte = (ListView) findViewById(R.id.list_view_reporte);

        List<String> meses = new ArrayList<String>(){{
            add("Enero");
            add("Febrero");
            add("Marzo");
            add("Abril");
            add("Mayo");
            add("Junio");
            add("Julio");
            add("Agosto");
            add("Septiembre");
            add("Octubre");
            add("Noviembre");
            add("Diciembre");
        }};

        spinner_mes.setAdapter(new ArrayAdapter<String>(ReportActivity.this, android.R.layout.simple_list_item_1, meses));

        if(years != null){
            spinner_anio.setAdapter(new ArrayAdapter<String>(
                    ReportActivity.this,
                    android.R.layout.simple_list_item_1,
                    years
            ));
        } else {
            ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
            final SafeAPI service = client.getService(SafeAPI.class, ReportActivity.this);
            Call<List<String>> call = service.getYears();
            call.enqueue(new Callback<List<String>>() {
                @Override
                public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                    if(response.code() == 200){
                        years = response.body();
                        spinner_anio.setAdapter(new ArrayAdapter<String>(
                                ReportActivity.this,
                                android.R.layout.simple_list_item_1,
                                years
                        ));
                    }
                }

                @Override
                public void onFailure(Call<List<String>> call, Throwable t) {

                }
            });
        }

        if(report != null){
            total_facturas.setText(String.valueOf(report.getTotalFacturas()));
            total_importe.setText(String.valueOf(report.getImporte()));
            total_traslados.setText(String.valueOf(report.getImpuestos()));

            empresas_reporte.setAdapter(new EmpresaReporteAdapter(
                    ReportActivity.this,
                    R.layout.list_item_empresa_reporte,
                    report.getEmpresas()
            ));
        }


        btn_mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String year = (String) spinner_anio.getSelectedItem();
                int month = spinner_mes.getSelectedItemPosition() + 1;

                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, ReportActivity.this);
                Call<Reporte> call = service.getReporte(year, month);
                call.enqueue(new Callback<Reporte>() {
                    @Override
                    public void onResponse(Call<Reporte> call, Response<Reporte> response) {
                        if(response.code() == 200){
                            report = response.body();
                            total_facturas.setText(String.valueOf(response.body().getTotalFacturas()));
                            total_importe.setText(String.valueOf(response.body().getImporte()));
                            total_traslados.setText(String.valueOf(response.body().getImpuestos()));

                            empresas_reporte.setAdapter(new EmpresaReporteAdapter(
                                    ReportActivity.this,
                                    R.layout.list_item_empresa_reporte,
                                    response.body().getEmpresas()
                            ));
                        }
                    }

                    @Override
                    public void onFailure(Call<Reporte> call, Throwable t) {

                    }
                });

            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(ReportActivity.this, MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
