package tk.facturadmin.safe.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.models.Factura;
import tk.facturadmin.safe.models.FacturaCompleta;
import tk.facturadmin.safe.models.requests.FacturaSeleccionada;
import tk.facturadmin.safe.models.requests.Ok;

public class FacturaActivity extends AppCompatActivity {

    Factura factura_simple;
    FacturaCompleta factura;
    int seleccionado_status, position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        factura = (FacturaCompleta) getIntent().getExtras().getSerializable("factura");
        factura_simple = (Factura) getIntent().getExtras().getSerializable("factura_simple");
        seleccionado_status = getIntent().getExtras().getInt("seleccionado");
        position = getIntent().getExtras().getInt("position");

        TextView folio = (TextView) findViewById(R.id.folio_val);
        TextView fecha = (TextView) findViewById(R.id.fecha_val);
        TextView no_certificado = (TextView) findViewById(R.id.no_certificado_val);
        TextView expedicion = (TextView) findViewById(R.id.expedicion_val);
        TextView comprobante = (TextView) findViewById(R.id.comprobante_val);
        TextView tipo_cambio = (TextView) findViewById(R.id.tipo_cambio_val);
        TextView moneda = (TextView) findViewById(R.id.moneda_val);
        TextView metodo_pago = (TextView) findViewById(R.id.metodo_pago_val);
        TextView forma_pago = (TextView) findViewById(R.id.forma_pago_val);
        TextView subtotal = (TextView) findViewById(R.id.subtotal_val);
        TextView total = (TextView) findViewById(R.id.total_val);
        TextView traslados = (TextView) findViewById(R.id.traslados_val);
        TextView emisor_rfc = (TextView) findViewById(R.id.emisor_rfc_val);
        TextView emisor_nombre = (TextView) findViewById(R.id.emisor_nombre_val);
        TextView emisor_domicilio = (TextView) findViewById(R.id.emisor_domicilio_val);
        TextView emisor_regimen = (TextView) findViewById(R.id.emisor_regimen_val);
        TextView receptor_rfc = (TextView) findViewById(R.id.receptor_rfc_val);
        TextView receptor_nombre = (TextView) findViewById(R.id.receptor_nombre_val);
        TextView receptor_domicilio = (TextView) findViewById(R.id.receptor_domicilio_val);

        folio.setText(String.valueOf(factura.getFolio()));
        fecha.setText(factura.getFecha().toString());
        no_certificado.setText(factura.getNoCertificado());
        expedicion.setText(factura.getLugarExpedicion());
        comprobante.setText(factura.getLugarExpedicion());
        tipo_cambio.setText(String.valueOf(factura.getTipoCambio()));
        moneda.setText(factura.getMoneda());
        metodo_pago.setText(factura.getMetodoDePago());
        forma_pago.setText(factura.getFormaDePago());
        subtotal.setText(String.valueOf(factura.getSubTotal()));
        total.setText(String.valueOf(factura.getTotal()));
        traslados.setText(String.valueOf(factura.getImpuestos().getTotalImpuestosTrasladados()));
        emisor_rfc.setText(factura.getEmisor().getRfc());
        emisor_nombre.setText(factura.getEmisor().getNombre());
        emisor_domicilio.setText(factura.getEmisor().getDomicilio().getDomicilio());
        emisor_regimen.setText(factura.getEmisor().getRegimenFiscal());
        receptor_rfc.setText(factura.getCliente().getRfc());
        receptor_nombre.setText(factura.getCliente().getNombre());
        receptor_domicilio.setText(factura.getCliente().getDomicilio().getDomicilio());

        final ImageButton btn_seleccionar = (ImageButton) findViewById(R.id.btn_seleccionar);
        ImageButton btn_pdf = (ImageButton) findViewById(R.id.btn_pdf);

        if(seleccionado_status == 1){
            btn_seleccionar.setColorFilter(getResources().getColor(R.color.green_checked));
        } else {
            btn_seleccionar.setColorFilter(getResources().getColor(R.color.gray_unchecked));
        }

        btn_seleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int seleccionar = (seleccionado_status == 1)?0:1;
                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, FacturaActivity.this);
                Call<Ok> call = service.seleccionarFactura(
                        new FacturaSeleccionada(
                                factura.getId(),
                                seleccionar
                        )
                );

                call.enqueue(new Callback<Ok>() {
                    @Override
                    public void onResponse(Call<Ok> call, Response<Ok> response) {
                        if(response.code() == 200){
                            seleccionado_status = seleccionar;
                            if(MainActivity.filter == 3){
                                if(seleccionar == 1){
                                    btn_seleccionar.setColorFilter(getResources().getColor(R.color.green_checked));
                                    MainActivity.facturas.add(factura_simple);
                                    position = MainActivity.facturas.size() -1;
                                } else {
                                    btn_seleccionar.setColorFilter(getResources().getColor(R.color.gray_unchecked));
                                    MainActivity.facturas.remove(position);
                                }
                            } else {
                                if(seleccionar == 1){
                                    btn_seleccionar.setColorFilter(getResources().getColor(R.color.green_checked));
                                } else {
                                    btn_seleccionar.setColorFilter(getResources().getColor(R.color.gray_unchecked));
                                }
                                MainActivity.facturas.get(position).setSeleccionado(seleccionar);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Ok> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        });

        if(factura.getFile().getPdf().equals("") || factura.getFile().getPdf() == null){
            btn_pdf.setVisibility(View.INVISIBLE);
        } else {
            btn_pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pdf = "http://facturadmin.tk/files/" + factura.getFile().getPdf();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(pdf), "application/pdf");
                    startActivity(intent);
                }
            });
        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
