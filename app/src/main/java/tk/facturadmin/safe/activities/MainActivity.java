package tk.facturadmin.safe.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.loopj.android.http.PersistentCookieStore;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.facturadmin.safe.R;
import tk.facturadmin.safe.adapters.FacturaCardAdapter;
import tk.facturadmin.safe.adapters.FacturaListener;
import tk.facturadmin.safe.apiclient.ApiClient;
import tk.facturadmin.safe.apiclient.SafeAPI;
import tk.facturadmin.safe.fragments.EmpresasFormFragment;
import tk.facturadmin.safe.fragments.FechaFormFragment;
import tk.facturadmin.safe.models.Empresa;
import tk.facturadmin.safe.models.Factura;
import tk.facturadmin.safe.models.requests.Ok;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FacturaListener, SwipeRefreshLayout.OnRefreshListener {

    public static List<Factura> facturas = null;
    public FacturaCardAdapter adapter;
    public static int filter = 0;
    public static String date_filter;
    public static int empresa_filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        ImageButton back = (ImageButton) header.findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        navigationView.setNavigationItemSelectedListener(this);

        final int columns =
                (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                ?1
                :2;

        final Context context = MainActivity.this;
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, columns));

        if(facturas != null){
            adapter = new FacturaCardAdapter(context, facturas, this);
            recyclerView.setAdapter(adapter);
        } else {
            facturas = new ArrayList<>();
            adapter = new FacturaCardAdapter(context, facturas, (FacturaListener) context);
            recyclerView.setAdapter(adapter);
            onFetch();
        }

        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        frameLayout.getBackground().setAlpha(0);
        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                frameLayout.getBackground().setAlpha(120);
                frameLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        fabMenu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                frameLayout.getBackground().setAlpha(0);
                frameLayout.setOnTouchListener(null);
            }
        });

        FloatingActionButton fab_report = (FloatingActionButton) findViewById(R.id.fab_report);
        FloatingActionButton fab_dowload_selected = (FloatingActionButton) findViewById(R.id.fab_dowload_selected);
        FloatingActionButton fab_help = (FloatingActionButton) findViewById(R.id.fab_help);

        fab_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ReportActivity.class);
                startActivity(intent);
            }
        });

        fab_dowload_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
                final SafeAPI service = client.getService(SafeAPI.class, MainActivity.this);
                Call<Ok> call = service.getSelectedPdfPath();
                call.enqueue(new Callback<Ok>() {
                    @Override
                    public void onResponse(Call<Ok> call, Response<Ok> response) {
                        if(response.code() == 200){
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse("http://facturadmin.tk" + response.body().response), "application/pdf");
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<Ok> call, Throwable t) {

                    }
                });
            }
        });

        fab_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pdf = "http://facturadmin.tk/user_guide";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(pdf), "application/pdf");
                startActivity(intent);
            }
        });


        SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.colorAccent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
            final SafeAPI service = client.getService(SafeAPI.class, MainActivity.this);
            Call<Ok> call = service.logout();
            call.enqueue(new Callback<Ok>() {
                @Override
                public void onResponse(Call<Ok> call, Response<Ok> response) {
                    if(response.code() == 200){
                        PersistentCookieStore store = new PersistentCookieStore(MainActivity.this);
                        store.clear();

                        finish();
                        Intent intent = new Intent();
                        intent.setClass(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<Ok> call, Throwable t) {

                }
            });


        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_all) {
            filter = 0;
            onFetch();
        } else if (id == R.id.nav_company) {
            filter = 1;
            EmpresasFormFragment empresasForm = new EmpresasFormFragment();
            empresasForm.setListener(this);
            empresasForm.show(getSupportFragmentManager(), "empresa_form");
        } else if (id == R.id.nav_date) {
            filter = 2;
            FechaFormFragment fechaForm = new FechaFormFragment();
            fechaForm.setListener(this);
            fechaForm.show(getSupportFragmentManager(), "fecha_form");

        } else if (id == R.id.nav_selected) {
            filter = 3;
            onFetch();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDataserUpdated(List<Factura> facturas_set){
        facturas.clear();
        facturas.addAll(facturas_set);
        adapter.notifyDataSetChanged();

        ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
        final SafeAPI service = client.getService(SafeAPI.class, MainActivity.this);
        Call<List<Empresa>> call_empresa = service.getEmpresas();
        call_empresa.enqueue(new Callback<List<Empresa>>() {
            @Override
            public void onResponse(Call<List<Empresa>> call, Response<List<Empresa>> response) {
                if(response.code() == 200){
                    EmpresasFormFragment.empresas = response.body();
                }
            }

            @Override
            public void onFailure(Call<List<Empresa>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onFetch() {
        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeLayout.setRefreshing(true);


        ApiClient client = new ApiClient(SafeAPI.ENDPOINT);
        final SafeAPI service = client.getService(SafeAPI.class, MainActivity.this);
        Call<List<Factura>> call = null;


        switch (filter){
            case 0:
                call = service.getFacturas();
                break;
            case 1:
                call = service.getFacturasByEmpresa(empresa_filter);
                break;
            case 2:
                call = service.getFacturasByDate(date_filter);
                break;
            case 3:
                call = service.getFacturasSeleccionadas();
                break;
            default:
                call = service.getFacturas();
                break;
        }

        call.enqueue(new Callback<List<Factura>>() {
            @Override
            public void onResponse(Call<List<Factura>> call, Response<List<Factura>> response) {
                if(response.code() == 200) {
                    onDataserUpdated(response.body());
                    swipeLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<Factura>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Algo salió mal", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemUnSelected(int position) {
        if(filter == 3){
            facturas.remove(position);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRefresh() {
        onFetch();
    }
}
